package com.example.cilaw.utils;

import java.io.Serializable;

public class MessagesItem implements Serializable {

    private String sendId;
    private String receiverId;
    private String messageTxt;
    private String timeStamp;
    private String myUid;

    public MessagesItem(String sendId, String receiverId, String messageTxt, String timeStamp, String myUid) {
        this.sendId = sendId;
        this.receiverId = receiverId;
        this.messageTxt = messageTxt;
        this.timeStamp = timeStamp;
        this.myUid = myUid;
    }

    public String getSendId() {
        return sendId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public String getMessageTxt() {
        return messageTxt;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String getMyUid() {
        return myUid;
    }
}
