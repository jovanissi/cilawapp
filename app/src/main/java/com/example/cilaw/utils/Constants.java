package com.example.cilaw.utils;

public class Constants {

    public static final String AUTHENTICATION_PREFS = "authentication_prefs";
    public static final String USER_PROFILE_PREFS = "user_profile_prefs";
    public static final String IS_ONLINE = "is_online";
    public static final String IS_OFFLINE = "is_offline";
    public static final String IS_CALLING = "is_calling";
    public static final String IS_BEING_CALLED = "is_being_called";

}
