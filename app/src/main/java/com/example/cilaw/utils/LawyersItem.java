package com.example.cilaw.utils;

import java.io.Serializable;

public class LawyersItem implements Serializable {

    private String iconUrl;
    private String name;
    private String phone;
    private String specialities;
    private String lawFirm;
    private String address;
    private String uid;

    public LawyersItem(String iconUrl, String name, String phone, String specialities, String lawFirm, String address, String uid) {
        this.iconUrl = iconUrl;
        this.name = name;
        this.phone = phone;
        this.specialities = specialities;
        this.lawFirm = lawFirm;
        this.address = address;
        this.uid = uid;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSpecialities() {
        return specialities;
    }

    public void setSpecialities(String specialities) {
        this.specialities = specialities;
    }

    public String getLawFirm() {
        return lawFirm;
    }

    public void setLawFirm(String lawFirm) {
        this.lawFirm = lawFirm;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
