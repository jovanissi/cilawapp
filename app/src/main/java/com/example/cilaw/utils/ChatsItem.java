package com.example.cilaw.utils;

import java.io.Serializable;

public class ChatsItem implements Serializable, Comparable< ChatsItem > {

    private String id;
    private String iconUrl;
    private String name;
    private String phone;
    private String lastMessage;
    private String lastMessageTime;

    public ChatsItem(String id, String iconUrl, String name, String phone, String lastMessage, String lastMessageTime) {
        this.id = id;
        this.iconUrl = iconUrl;
        this.name = name;
        this.phone = phone;
        this.lastMessage = lastMessage;
        this.lastMessageTime = lastMessageTime;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public String getLastMessageTime() {
        return lastMessageTime;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getId() {
        return id;
    }

    @Override
    public int compareTo(ChatsItem o) {
        return this.getLastMessageTime().compareTo(o.getLastMessageTime());
    }
}
