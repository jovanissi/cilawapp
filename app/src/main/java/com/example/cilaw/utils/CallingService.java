package com.example.cilaw.utils;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.cilaw.activities.NewCallActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import static com.example.cilaw.utils.Constants.IS_BEING_CALLED;
import static com.example.cilaw.utils.Constants.IS_CALLING;
import static com.example.cilaw.utils.Constants.IS_OFFLINE;
import static com.example.cilaw.utils.Constants.IS_ONLINE;
import static com.example.cilaw.utils.Constants.USER_PROFILE_PREFS;

public class CallingService extends Service {

    private SharedPreferences userProfilePrefs;

    private FirebaseUser user;
    private FirebaseDatabase db;

    private String UID;
    private String type;

    private String caller_id = "", called_id = "";
    private boolean calling, called;

    public CallingService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        userProfilePrefs = getSharedPreferences(USER_PROFILE_PREFS, MODE_PRIVATE);

        db = FirebaseDatabase.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {
            UID = user.getUid();

            type = userProfilePrefs.getString("type", "");

//            LocalBroadcastManager.getInstance(this).registerReceiver(is_calling, new IntentFilter(IS_CALLING));
//            LocalBroadcastManager.getInstance(this).registerReceiver(is_being_called, new IntentFilter(IS_BEING_CALLED));

            LocalBroadcastManager.getInstance(this).registerReceiver(call_ended, new IntentFilter("call_ended"));

            if (type.equalsIgnoreCase("citizen")) {

                try {

                    db.getReference("Citizens").child(UID).child("video_call").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            try {

                                String activity = dataSnapshot.child("activity").getValue().toString();
                                String caller_id = dataSnapshot.child("caller_id").getValue().toString();
                                String caller_name = dataSnapshot.child("caller_name").getValue().toString();

                                if (activity.equalsIgnoreCase("being_called")) {
                                    db.getReference("Citizens").child(UID).child("video_call").child("on_call").setValue("true");

                                    Intent intent = new Intent(getApplicationContext(), NewCallActivity.class);
                                    intent.putExtra("caller_id", caller_id);
                                    intent.putExtra("caller_name", caller_name);
                                    intent.setAction(Intent.ACTION_VIEW);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    getApplicationContext().startActivity(intent);
                                }

                            } catch (Exception e) {
                                Log.d("Error: ", "onDataChange: ");
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                } catch (Exception e) {
                    Log.d("CallingService", e.getMessage());
                }

            } else if (type.equalsIgnoreCase("lawyer")) {

                try {

                    db.getReference("Lawyers").child(UID).child("video_call").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            try {

                                String activity = dataSnapshot.child("activity").getValue().toString();
                                String caller_id = dataSnapshot.child("caller_id").getValue().toString();

                                if (activity.equalsIgnoreCase("being_called")) {
                                    db.getReference("Citizens").child(UID).child("video_call").child("on_call").setValue("true");

                                    Intent intent = new Intent(getApplicationContext(), NewCallActivity.class);
                                    intent.putExtra("caller_id", caller_id);
                                    intent.setAction(Intent.ACTION_VIEW);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    getApplicationContext().startActivity(intent);
                                }

                            } catch (Exception e) {
                                Log.d("CallingService", e.getMessage());
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                } catch (Exception e) {
                    Log.d("CallingService", e.getMessage());
                }

            }

        }
    }

    private BroadcastReceiver call_ended = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            caller_id = intent.getStringExtra("caller_id");
            called_id = intent.getStringExtra("called_id");
            calling = intent.getBooleanExtra("calling", false);
            called = intent.getBooleanExtra("called", false);

            Map<String, String> params = new HashMap<>();
            params.put("activity", "Null");
            params.put("caller_id", "Null");
            params.put("caller_name", "Null");
            params.put("on_call", "false");

            if (calling) {

                if (type.equalsIgnoreCase("citizen")) {

                    db.getReference("Lawyers").child(called_id).child("video_call").setValue(params);
                    db.getReference("Citizens").child(caller_id).child("video_call").setValue(params);

                } else if (type.equalsIgnoreCase("lawyer")) {

                    db.getReference("Citizens").child(called_id).child("video_call").setValue(params);
                    db.getReference("Lawyers").child(caller_id).child("video_call").setValue(params);

                }

            } else if (called) {

                if (type.equalsIgnoreCase("citizen")) {

                    db.getReference("Lawyers").child(caller_id).child("video_call").setValue(params);
                    db.getReference("Citizens").child(called_id).child("video_call").setValue(params);

                } else if (type.equalsIgnoreCase("lawyer")) {

                    db.getReference("Citizens").child(caller_id).child("video_call").setValue(params);
                    db.getReference("Lawyers").child(called_id).child("video_call").setValue(params);

                }

            }

        }
    };

    private BroadcastReceiver is_calling = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (type.equalsIgnoreCase("citizen")) {

                Map<String, String> params = new HashMap<>();
                params.put("activity", "calling");

                try {

                    db.getReference("Citizens").child(UID).child("video_call").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            String activity = dataSnapshot.child("activity").getValue().toString();
                            String caller_id = dataSnapshot.child("caller_id").getValue().toString();
                            String caller_name = dataSnapshot.child("caller_name").getValue().toString();

                            if (activity.equalsIgnoreCase("being_called")) {
                                db.getReference("Citizens").child(UID).child("video_call").child("on_call").setValue("true");

                                Intent intent = new Intent(getApplicationContext(), NewCallActivity.class);
                                intent.putExtra("caller_id", caller_id);
                                intent.putExtra("caller_name", caller_name);
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                getApplicationContext().startActivity(intent);
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                } catch (Exception e) {
                    Log.d("CallingService", e.getMessage());
                }

            } else if (type.equalsIgnoreCase("lawyer")) {

                try {

                    db.getReference("Lawyers").child(UID).child("video_call").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            String activity = dataSnapshot.child("activity").getValue().toString();
                            String caller_id = dataSnapshot.child("caller_id").getValue().toString();
                            String caller_name = dataSnapshot.child("caller_name").getValue().toString();

                            if (activity.equalsIgnoreCase("being_called")) {
                                db.getReference("Citizens").child(UID).child("video_call").child("on_call").setValue("true");

                                Intent intent = new Intent(getApplicationContext(), NewCallActivity.class);
                                intent.putExtra("caller_id", caller_id);
                                intent.putExtra("caller_name", caller_name);
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                getApplicationContext().startActivity(intent);
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                } catch (Exception e) {
                    Log.d("CallingService", e.getMessage());
                }

            }

        }
    };

    private BroadcastReceiver is_being_called = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (type.equalsIgnoreCase("citizen")) {

                try {

                    db.getReference("Citizens").child(UID).child("video_call").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            String activity = dataSnapshot.child("activity").getValue().toString();
                            String caller_id = dataSnapshot.child("caller_id").getValue().toString();
                            String caller_name = dataSnapshot.child("caller_name").getValue().toString();

                            if (activity.equalsIgnoreCase("being_called")) {
                                db.getReference("Citizens").child(UID).child("video_call").child("on_call").setValue("true");

                                Intent intent = new Intent(getApplicationContext(), NewCallActivity.class);
                                intent.putExtra("caller_id", caller_id);
                                intent.putExtra("caller_name", caller_name);
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                getApplicationContext().startActivity(intent);
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                } catch (Exception e) {
                    Log.d("CallingService", e.getMessage());
                }

            } else if (type.equalsIgnoreCase("lawyer")) {

                try {

                    db.getReference("Lawyers").child(UID).child("video_call").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            String activity = dataSnapshot.child("activity").getValue().toString();
                            String caller_id = dataSnapshot.child("caller_id").getValue().toString();

                            if (activity.equalsIgnoreCase("being_called")) {
                                db.getReference("Citizens").child(UID).child("video_call").child("on_call").setValue("true");

                                Intent intent = new Intent(getApplicationContext(), NewCallActivity.class);
                                intent.putExtra("caller_id", caller_id);
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                getApplicationContext().startActivity(intent);
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                } catch (Exception e) {
                    Log.d("CallingService", e.getMessage());
                }

            }

        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(is_calling != null){
            unregisterReceiver(is_calling);
        }
        if(is_being_called != null){
            unregisterReceiver(is_being_called);
        }
        if (call_ended != null) {
            unregisterReceiver(call_ended);
        }
    }
}
