package com.example.cilaw.utils;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import static com.example.cilaw.utils.Constants.IS_OFFLINE;
import static com.example.cilaw.utils.Constants.IS_ONLINE;
import static com.example.cilaw.utils.Constants.USER_PROFILE_PREFS;

public class MyService extends Service {

    private SharedPreferences userProfilePrefs;

    private FirebaseUser user;
    private FirebaseDatabase db;

    private String UID;
    private String type;

    public MyService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        userProfilePrefs = getSharedPreferences(USER_PROFILE_PREFS, MODE_PRIVATE);

        user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {
            db = FirebaseDatabase.getInstance();
            UID = user.getUid();

            type = userProfilePrefs.getString("type", "");

            this.registerReceiver(networkStateChanged, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            LocalBroadcastManager.getInstance(this).registerReceiver(is_online, new IntentFilter(IS_ONLINE));
            LocalBroadcastManager.getInstance(this).registerReceiver(is_offline, new IntentFilter(IS_OFFLINE));
        }

    }

    private BroadcastReceiver networkStateChanged = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (NetworkChecker.isConnected(MyService.this)) {
                //connected :)

//                Map<String, String> params = new HashMap<>();
//                params.put(IS_ONLINE, "true");

                if (type.equalsIgnoreCase("citizen")) {
                    db.getReference("Citizens").child(UID).child("profile").child(IS_ONLINE).setValue("true");
                } else if (type.equalsIgnoreCase("lawyer")) {
                    db.getReference("Lawyers").child(UID).child("profile").child(IS_ONLINE).setValue("true");
                }

            } else {
                // Not connected :(

                if (type.equalsIgnoreCase("citizen")) {
                    db.getReference("Citizens").child(UID).child("profile").child(IS_ONLINE).setValue("false");
                } else if (type.equalsIgnoreCase("lawyer")) {
                    db.getReference("Lawyers").child(UID).child("profile").child(IS_ONLINE).setValue("false");
                }
            }
        }
    };

    private BroadcastReceiver is_online = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (type.equalsIgnoreCase("citizen")) {
                db.getReference("Citizens").child(UID).child("profile").child(IS_ONLINE).setValue("true");
            } else if (type.equalsIgnoreCase("lawyer")) {
                db.getReference("Lawyers").child(UID).child("profile").child(IS_ONLINE).setValue("true");
            }

        }
    };

    private BroadcastReceiver is_offline = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (type.equalsIgnoreCase("citizen")) {
                db.getReference("Citizens").child(UID).child("profile").child(IS_ONLINE).setValue("false");
            } else if (type.equalsIgnoreCase("lawyer")) {
                db.getReference("Lawyers").child(UID).child("profile").child(IS_ONLINE).setValue("false");
            }

        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(is_online != null){
            unregisterReceiver(is_online);
        }
        if(is_offline != null){
            unregisterReceiver(is_offline);
        }
        if(networkStateChanged != null){
            unregisterReceiver(networkStateChanged);
        }
    }
}
