package com.example.cilaw.fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.cilaw.R;
import com.example.cilaw.activities.LoginActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;
import static com.example.cilaw.utils.Constants.AUTHENTICATION_PREFS;
import static com.example.cilaw.utils.Constants.IS_ONLINE;
import static com.example.cilaw.utils.Constants.USER_PROFILE_PREFS;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    @BindView(R.id.icon) CircleImageView icon;
    @BindView(R.id.iconProgressBar) ProgressBar iconProgressBar;
    @BindView(R.id.fullName) EditText fullName;
    @BindView(R.id.phone_view) EditText phone_view;
    @BindView(R.id.address_view) EditText address_view;
    @BindView(R.id.forLawyersView) LinearLayout forLawyersView;
    @BindView(R.id.specialities_view) EditText specialities_view;
    @BindView(R.id.law_firm_view) EditText law_firm_view;
    @BindView(R.id.logout) Button logout;
    @BindView(R.id.nid_view) EditText nid_view;
    @BindView(R.id.certified_text_view) TextView certified_text_view;
    @BindView(R.id.not_certified_text_view) TextView not_certified_text_view;

    private SharedPreferences userProfilePrefs, authPrefs;

    private FirebaseAuth mAuth;
    private FirebaseDatabase db;
    private String UID;

    private String type;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        ButterKnife.bind(this, v);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance();
        UID = mAuth.getCurrentUser().getUid();

        authPrefs = this.getActivity().getSharedPreferences(AUTHENTICATION_PREFS, MODE_PRIVATE);
        userProfilePrefs = this.getActivity().getSharedPreferences(USER_PROFILE_PREFS, MODE_PRIVATE);

        type = userProfilePrefs.getString("type", "");

        fullName.setText(userProfilePrefs.getString("name", ""));
        address_view.setText(userProfilePrefs.getString("address", ""));
        phone_view.setText(userProfilePrefs.getString("phone", ""));

        String type = userProfilePrefs.getString("type", "");

        if (type.trim().equalsIgnoreCase("lawyer")){
            forLawyersView.setVisibility(View.VISIBLE);
            specialities_view.setText(userProfilePrefs.getString("specialities", ""));
            law_firm_view.setText(userProfilePrefs.getString("law_firm", ""));
            nid_view.setText(userProfilePrefs.getString("nid_number", ""));

        } else {
            forLawyersView.setVisibility(View.GONE);
        }

        try {

            Picasso.with(getActivity()).load(userProfilePrefs.getString("icon_url", "")).placeholder(R.drawable.pp).into(icon);

        } catch (Exception e) {}

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });

        return v;
    }

    private void logout() {

//        if (type.equalsIgnoreCase("citizen")) {
//            db.getReference("Citizens").child(UID).child("profile").child(IS_ONLINE).setValue("false");
//        } else if (type.equalsIgnoreCase("lawyer")) {
//            db.getReference("Lawyers").child(UID).child("profile").child(IS_ONLINE).setValue("false");
//        }

        userProfilePrefs.edit()
                .putString("name", "")
                .putString("icon_url", "")
                .putString("type", "")
                .putString("phone", "")
                .putString("address", "")
                .putString("law_firm", "")
                .putString("specialities", "")
                .putString("nid_number", "")
                .putString("nid_pic_url", "")
                .putString("certificate_pic_url", "")
                .apply();

        mAuth.signOut();

        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

}
