package com.example.cilaw.fragments;


import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.cilaw.R;
import com.example.cilaw.adapters.ChatsAdapter;
import com.example.cilaw.utils.ChatsItem;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;
import static com.example.cilaw.utils.Constants.USER_PROFILE_PREFS;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatsFragment extends Fragment {

    @BindView(R.id.rootView) SwipeRefreshLayout rootView;
    @BindView(R.id.recycleView) RecyclerView recycleView;

    private SharedPreferences userProfilePrefs;

    private FirebaseUser user;
    private FirebaseDatabase db;
    private String UID;

    List<ChatsItem> chatsItemList = new ArrayList<>();

    public ChatsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_chats, container, false);
        ButterKnife.bind(this, v);

        user = FirebaseAuth.getInstance().getCurrentUser();
        db = FirebaseDatabase.getInstance();
        UID = user.getUid();

        userProfilePrefs = this.getActivity().getSharedPreferences(USER_PROFILE_PREFS, MODE_PRIVATE);

        recycleView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recycleView.setLayoutManager(linearLayoutManager);

        // Swipe down to refresh the list of others
        rootView.setColorSchemeResources(R.color.colorPrimary);
        rootView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadChats();
                rootView.setRefreshing(true);
            }
        });

        return v;
    }

    private void loadChats() {

        rootView.setRefreshing(true);

        String type = userProfilePrefs.getString("type", "");

        if (type.equalsIgnoreCase("citizen")) {

            try {
                db.getReference("Citizens").child(UID).child("chats").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        rootView.setRefreshing(false);

                        chatsItemList.clear();

                        for (DataSnapshot chatSnapshot : dataSnapshot.getChildren()) {

                            try {

                                String name = chatSnapshot.child("profile_and_data").child("name").getValue().toString();
                                String iconUrl = chatSnapshot.child("profile_and_data").child("icon_url").getValue().toString();
                                String lastMessage = chatSnapshot.child("profile_and_data").child("last_message").getValue().toString();
                                String timeStamp = chatSnapshot.child("profile_and_data").child("last_timeStamp").getValue().toString();
                                String phone = chatSnapshot.child("profile_and_data").child("phone").getValue().toString();
                                String id = chatSnapshot.getKey();

                                chatsItemList.add(new ChatsItem(id, iconUrl, name, phone, lastMessage, timeStamp));

                            } catch (Exception e) {}

                        }

                        Collections.sort(chatsItemList);
                        recycleView.setAdapter(new ChatsAdapter(getActivity(), chatsItemList));

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            } catch (Exception e) {}

        } else if (type.equalsIgnoreCase("lawyer")) {

            try{
                db.getReference("Lawyers").child(UID).child("chats").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        rootView.setRefreshing(false);

                        chatsItemList.clear();

                        for (DataSnapshot chatSnapshot : dataSnapshot.getChildren()) {

                            try {

                                String name = chatSnapshot.child("profile_and_data").child("name").getValue().toString();
                                String iconUrl = chatSnapshot.child("profile_and_data").child("icon_url").getValue().toString();
                                String lastMessage = chatSnapshot.child("profile_and_data").child("last_message").getValue().toString();
                                String timeStamp = chatSnapshot.child("profile_and_data").child("last_timeStamp").getValue().toString();
                                String phone = chatSnapshot.child("profile_and_data").child("phone").getValue().toString();
                                String id = chatSnapshot.getKey();

                                chatsItemList.add(new ChatsItem(id, iconUrl, name, phone, lastMessage, timeStamp));

                            } catch (Exception e) {}

                        }

                        recycleView.setAdapter(new ChatsAdapter(getActivity(), chatsItemList));

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            } catch (Exception e) {}

        }

    }

    @Override
    public void onStart() {
        super.onStart();

        if (user != null) {
            loadChats();
        }
    }
}
