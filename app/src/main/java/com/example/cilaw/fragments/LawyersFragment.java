package com.example.cilaw.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.cilaw.R;
import com.example.cilaw.adapters.LawyersAdapter;
import com.example.cilaw.utils.LawyersItem;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;
import static com.example.cilaw.utils.Constants.USER_PROFILE_PREFS;

/**
 * A simple {@link Fragment} subclass.
 */
public class LawyersFragment extends Fragment {

    @BindView(R.id.rootView) SwipeRefreshLayout rootView;
    @BindView(R.id.recycleView) RecyclerView recycleView;
    @BindView(R.id.searchView) SearchView searchView;

    private SharedPreferences userProfilePrefs;

    private FirebaseUser user;
    private FirebaseDatabase db;

    List<LawyersItem> lawyersItemList = new ArrayList<>();

    public LawyersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_lawyers, container, false);
        ButterKnife.bind(this, v);

        user = FirebaseAuth.getInstance().getCurrentUser();
        db = FirebaseDatabase.getInstance();

        userProfilePrefs = this.getActivity().getSharedPreferences(USER_PROFILE_PREFS, MODE_PRIVATE);

        recycleView.setHasFixedSize(true);
        recycleView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // Swipe down to refresh the list of others
        rootView.setColorSchemeResources(R.color.colorPrimary);
        rootView.setOnRefreshListener(() -> {
            loadLawyers();
            rootView.setRefreshing(true);
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                String[] splittedInput = s.split(" ", 5);

                loadLawyers(splittedInput);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        searchView.setOnCloseListener(() -> {

            loadLawyers();

            return false;
        });

        return v;
    }

    private void loadLawyers() {

        rootView.setRefreshing(true);
        searchView.setEnabled(false);

        db.getReference("Lawyers").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                rootView.setRefreshing(false);
                searchView.setEnabled(true);

                lawyersItemList.clear();

//                Log.d("Lawyers", dataSnapshot.toString());

                for (DataSnapshot lawyerSnapshot: dataSnapshot.getChildren()) {

//                    Log.d("Lawyers", lawyerSnapshot.toString());

                    try {

                        String name = lawyerSnapshot.child("profile").child("name").getValue().toString();
                        String phone = lawyerSnapshot.child("profile").child("phone").getValue().toString();
                        String address = lawyerSnapshot.child("profile").child("address").getValue().toString();
                        String specialities = lawyerSnapshot.child("profile").child("specialities").getValue().toString();
                        String law_firm = lawyerSnapshot.child("profile").child("law_firm").getValue().toString();
                        String icon_url = lawyerSnapshot.child("profile").child("icon_url").getValue().toString();
                        String uid = lawyerSnapshot.getKey();
                        String certified = lawyerSnapshot.child("profile").child("certified").getValue().toString();

                        if (certified.equalsIgnoreCase("true")) {
                            lawyersItemList.add(new LawyersItem(icon_url, name, phone, specialities, law_firm, address, uid));
                        }

                    } catch (Exception e) {}

                }
//
                recycleView.setAdapter(new LawyersAdapter(getActivity(), lawyersItemList));

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("Lawyers error: ", databaseError.getMessage());
            }
        });

    }

    private void loadLawyers(String[] splittedInput) {
        rootView.setRefreshing(true);
        searchView.setEnabled(false);

        db.getReference("Lawyers").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                rootView.setRefreshing(false);
                searchView.setEnabled(true);

                lawyersItemList.clear();

                for (DataSnapshot lawyerSnapshot: dataSnapshot.getChildren()) {

                    try {
                        String name = lawyerSnapshot.child("profile").child("name").getValue().toString();
                        String specialities = lawyerSnapshot.child("profile").child("specialities").getValue().toString();
                        String[] splittedNames = name.split(" ", 5);
                        String[] splittedSpecs = specialities.split(" ", 5);
                        String[] searchables = combine(splittedNames, splittedSpecs);

//                        String[] names = name.split(" ");

                        for (String s : splittedInput) {

                            for (String searchable : searchables) {

                                if (s.trim().equalsIgnoreCase(searchable)) {

                                    String phone = lawyerSnapshot.child("profile").child("phone").getValue().toString();
                                    String address = lawyerSnapshot.child("profile").child("address").getValue().toString();
                                    String law_firm = lawyerSnapshot.child("profile").child("law_firm").getValue().toString();
                                    String icon_url = lawyerSnapshot.child("profile").child("icon_url").getValue().toString();
                                    String uid = lawyerSnapshot.getKey();
                                    String certified = lawyerSnapshot.child("profile").child("certified").getValue().toString();

                                    if (certified.equalsIgnoreCase("true")) {
                                        lawyersItemList.add(new LawyersItem(icon_url, name, phone, specialities, law_firm, address, uid));
                                    }
                                }

                            }

                        }

//                        boolean b = lawyerName.trim().equalsIgnoreCase(name.trim());
//
//                        if (b || lawyerName.trim().equalsIgnoreCase(names[0].trim()) || lawyerName.trim().equalsIgnoreCase(names[1].trim())) {
//                            String phone = lawyerSnapshot.child("profile").child("phone").getValue().toString();
//                            String address = lawyerSnapshot.child("profile").child("address").getValue().toString();
//                            String law_firm = lawyerSnapshot.child("profile").child("law_firm").getValue().toString();
//                            String icon_url = lawyerSnapshot.child("profile").child("icon_url").getValue().toString();
//                            String uid = lawyerSnapshot.getKey();
//
//                            lawyersItemList.add(new LawyersItem(icon_url, name, phone, specialities, law_firm, address, uid));
//                        }

                    } catch (Exception e) {}

                }
//
                recycleView.setAdapter(new LawyersAdapter(getActivity(), lawyersItemList));

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("Lawyers error: ", databaseError.getMessage());
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        if (user != null) {
            loadLawyers();
        }
    }

    public static String[] combine(String[] a, String[] b){
        int length = a.length + b.length;
        String[] result = new String[length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);

        return result;
    }

}
