package com.example.cilaw.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.cilaw.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import static com.example.cilaw.utils.Constants.USER_PROFILE_PREFS;

public class NewCallActivity extends AppCompatActivity {

    private Context context;

    private FirebaseUser user;
    private FirebaseDatabase db;
    private String UID = "";

    private String caller_id, caller_name;
    private SharedPreferences userProfilePrefs;
    private String type;

    TextView callerName;

    Ringtone ringtone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_call);

        context = this;

        user = FirebaseAuth.getInstance().getCurrentUser();
        db = FirebaseDatabase.getInstance();
        userProfilePrefs = getSharedPreferences(USER_PROFILE_PREFS, MODE_PRIVATE);

        callerName = (TextView) findViewById(R.id.callerName);

        if (user != null) {
            UID = user.getUid();

            type = userProfilePrefs.getString("type", "");
            caller_id = getIntent().getStringExtra("caller_id");
            caller_name = getIntent().getStringExtra("caller_name");

            callerName.setText(caller_name);

            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            ringtone = RingtoneManager.getRingtone(context,uri);
            ringtone.play();

        } else {

        }
    }

    @Override
    public void onBackPressed() {

    }


    public void onEndCallClicked(View view) {

        Map<String, String> params = new HashMap<>();
        params.put("activity", "Null");
        params.put("caller_id", "Null");
        params.put("caller_name", "Null");
        params.put("on_call", "false");

        if (type.equalsIgnoreCase("citizen")) {

            db.getReference("Citizens").child(UID).child("video_call").setValue(params);
            db.getReference("Lawyers").child(caller_id).child("video_call").setValue(params);

        } else if (type.equalsIgnoreCase("lawyer")) {

            db.getReference("Lawyers").child(UID).child("video_call").setValue(params);
            db.getReference("Citizens").child(caller_id).child("video_call").setValue(params);

        }

        ringtone.stop();
        finish();
    }

    public void onAcceptCallClicked(View view) {
        ringtone.stop();

        Intent intent = new Intent(context, VideoChatAgoraActivity.class);
        intent.putExtra("caller_id", caller_id);
        intent.putExtra("caller_name", caller_name);
        intent.putExtra("called", true);
        startActivity(intent);
        finish();
    }

}
