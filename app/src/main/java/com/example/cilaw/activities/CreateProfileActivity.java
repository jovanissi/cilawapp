package com.example.cilaw.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.cilaw.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.cilaw.utils.Constants.AUTHENTICATION_PREFS;
import static com.example.cilaw.utils.Constants.USER_PROFILE_PREFS;

public class CreateProfileActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.icon) CircleImageView icon;
    @BindView(R.id.iconProgressBar) ProgressBar iconProgressBar;
    @BindView(R.id.fullName) EditText fullName;
    @BindView(R.id.phone_view) EditText phone_view;
    @BindView(R.id.radioButtons) RadioGroup radioButtons;
    @BindView(R.id.ordinaryRadio) RadioButton ordinaryRadio;
    @BindView(R.id.lawyerRadio) RadioButton lawyerRadio;
    @BindView(R.id.address_view) EditText address_view;
    @BindView(R.id.forLawyersView) LinearLayout forLawyersView;
    @BindView(R.id.specialities_view) EditText specialities_view;
    @BindView(R.id.law_firm_view) EditText law_firm_view;
    @BindView(R.id.progress) ProgressBar progress;
    @BindView(R.id.register_btn) Button register_btn;
    @BindView(R.id.nid_view) EditText nid_view;
    @BindView(R.id.uploadNidPicBtn) Button uploadNidPicBtn;
    @BindView(R.id.nidPic) ImageView nidPic;
    @BindView(R.id.nidPicProgressBar) ProgressBar nidPicProgressBar;
    @BindView(R.id.uploadNidCertificateBtn) Button uploadNidCertificateBtn;
    @BindView(R.id.certificatePic) ImageView certificatePic;
    @BindView(R.id.certificatePicProgressBar) ProgressBar certificatePicProgressBar;

    private String fullNameStr = "", phoneStr = "", addressStr = "", specilitiesStr = "", lawFirmStr = "", nidStr = "";
    private boolean isCitizen = true, isLawyer = false;
    private String UID;

    private Context context;

    private FirebaseUser user;
    private StorageReference storageReference;
    private FirebaseDatabase db;

    private Uri filePath;
    private final int PICK_IMAGE_REQUEST = 71;
    private final int NID_PICK_IMAGE_REQUEST = 72;
    private final int CERTIFICATE_PICK_IMAGE_REQUEST = 73;
    private String generatedIconPath = "Null";
    private String generatedNicPicPath = "null";
    private String generatedCertPicPath = "null";

    private SharedPreferences userProfilePrefs, authPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.setTitle("Create profile");

        context = this;

        authPrefs = getSharedPreferences(AUTHENTICATION_PREFS, MODE_PRIVATE);
        userProfilePrefs = getSharedPreferences(USER_PROFILE_PREFS, MODE_PRIVATE);

        db = FirebaseDatabase.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        user = FirebaseAuth.getInstance().getCurrentUser();
        UID = user.getUid();

        phoneStr = getIntent().getStringExtra("phone");
        phone_view.setText(phoneStr);

//        if (ordinaryRadio.isChecked()) {
//
//
//        } else if (lawyerRadio.isChecked()) {
//
//
//        }

        ordinaryRadio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forLawyersView.setVisibility(View.GONE);
                isCitizen = true;
                isLawyer = false;
            }
        });

        lawyerRadio.setOnClickListener(v -> {
            forLawyersView.setVisibility(View.VISIBLE);
            isLawyer = true;
            isCitizen = false;
        });

        // choosing the restaurant icon
        icon.setOnClickListener(v -> chooseImage());

        register_btn.setOnClickListener(v -> checkInputs());

        uploadNidPicBtn.setOnClickListener(view -> {
            uploadNidPicBtn.setVisibility(View.GONE);
            chooseNidImage();
        });

        uploadNidCertificateBtn.setOnClickListener(view -> {
            uploadNidCertificateBtn.setVisibility(View.GONE);
            chooseCertImage();
        });

        nidPic.setOnClickListener(view -> {
            uploadNidPicBtn.setVisibility(View.GONE);
            nidPic.setVisibility(View.GONE);
            chooseNidImage();
        });

        certificatePic.setOnClickListener(view -> {
            uploadNidCertificateBtn.setVisibility(View.GONE);
            certificatePic.setVisibility(View.GONE);
            chooseCertImage();
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void setState(String state){

        if (state.equals("loading")){
            icon.setEnabled(false);
            fullName.setEnabled(false);
            phone_view.setEnabled(false);
            radioButtons.setEnabled(false);
            address_view.setEnabled(false);
            specialities_view.setEnabled(false);
            law_firm_view.setEnabled(false);
            nid_view.setEnabled(false);
            nidPic.setEnabled(false);
            uploadNidPicBtn.setEnabled(false);
            certificatePic.setEnabled(false);
            uploadNidCertificateBtn.setEnabled(false);
            register_btn.setVisibility(View.INVISIBLE);
            progress.setVisibility(View.VISIBLE);
        }

        else if (state.equals("idle")){
            icon.setEnabled(true);
            fullName.setEnabled(true);
            phone_view.setEnabled(true);
            radioButtons.setEnabled(true);
            address_view.setEnabled(true);
            specialities_view.setEnabled(true);
            law_firm_view.setEnabled(true);
            nid_view.setEnabled(true);
            nidPic.setEnabled(true);
            uploadNidPicBtn.setEnabled(true);
            certificatePic.setEnabled(true);
            uploadNidCertificateBtn.setEnabled(true);
            register_btn.setVisibility(View.VISIBLE);
            progress.setVisibility(View.INVISIBLE);
        }
    }

    private void checkInputs(){
        fullNameStr = fullName.getText().toString();
        addressStr = address_view.getText().toString();
        specilitiesStr = specialities_view.getText().toString();
        lawFirmStr = law_firm_view.getText().toString();
        nidStr = nid_view.getText().toString();

        if (lawyerRadio.isChecked()) {

            if (TextUtils.isEmpty(specilitiesStr.trim())) {
                specialities_view.setError("Field Required");
            } else if (TextUtils.isEmpty(lawFirmStr.trim())) {
                law_firm_view.setError("Field Required");
            } else if (TextUtils.isEmpty(nidStr.trim())) {
                nid_view.setError("Field Required");
            } else if (generatedNicPicPath.equalsIgnoreCase("null")) {
                Toast.makeText(context, "No NID or Passport uploaded", Toast.LENGTH_SHORT).show();
            } else if (generatedCertPicPath.equalsIgnoreCase("null")) {
                Toast.makeText(context, "No Certification uploaded", Toast.LENGTH_SHORT).show();
            }
        }

        if (TextUtils.isEmpty(fullNameStr)){
            fullName.setError("Name Required");
        }

        else if (TextUtils.isEmpty(addressStr)){
            address_view.setError("Address Required");
        }

        else {
            setState("loading");
            register();
        }
    }

    // choosing (pick) an image from the phone's gallery
    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
    }

    private void chooseNidImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), NID_PICK_IMAGE_REQUEST);
    }

    private void chooseCertImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), CERTIFICATE_PICK_IMAGE_REQUEST);
    }

    // What happens after the image is picked
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();

            if (filePath != null) {

                // Uploading the image to firebase storage
                storageReference.child("profile_icons/" + UID + "/" + "icon.jpg")
                        .putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // When the picked image has been uploaded successfully

                        iconProgressBar.setVisibility(View.GONE);

                        storageReference.child("profile_icons/" + UID + "/" + "icon.jpg")
                                .getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                generatedIconPath = uri.toString();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

                        // Get the generated path of the uploaded image
//                        try {
//                            Uri downloadUri = taskSnapshot.getDownloadUrl();
//                            generatedIconPath = downloadUri.toString();
//                        } catch (Exception e){
//                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
//                        }

                        // Showing the image in the restIcon view
                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                            icon.setImageBitmap(bitmap);
                        } catch (IOException e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // When the uploading the picked image has failed
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        // When the picked image is being uploaded

                        // Showing the progress of the upload
                        iconProgressBar.setVisibility(View.VISIBLE);
                        double progress = (100 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            iconProgressBar.setProgress((int) progress, true);
                        } else {
                            iconProgressBar.setProgress((int) progress);
                        }
                    }
                });
            }
        }

        else if (requestCode == NID_PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();

            if (filePath != null) {

                // Uploading the image to firebase storage
                storageReference.child("nid_pics/" + UID + "/" + "nid_pic.jpg")
                        .putFile(filePath)
                        .addOnSuccessListener(taskSnapshot -> {
                            // When the picked image has been uploaded successfully

                            nidPicProgressBar.setVisibility(View.GONE);

                            storageReference.child("nid_pics/" + UID + "/" + "nid_pic.jpg")
                                    .getDownloadUrl()
                                    .addOnSuccessListener(uri -> generatedNicPicPath = uri.toString())
                                    .addOnFailureListener(e -> Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show());

                            // Showing the image in the restIcon view
                            try {
                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                                nidPic.setImageBitmap(bitmap);
                                nidPic.setVisibility(View.VISIBLE);
                            } catch (IOException e) {
                                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                                uploadNidPicBtn.setVisibility(View.VISIBLE);
                            }
                        })
                        .addOnFailureListener(e -> {
                            // When the uploading the picked image has failed
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                            uploadNidPicBtn.setVisibility(View.VISIBLE);
                        })
                        .addOnProgressListener(taskSnapshot -> {
                            // When the picked image is being uploaded

                            // Showing the progress of the upload
                            nidPicProgressBar.setVisibility(View.VISIBLE);
                            double progress = (100 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                nidPicProgressBar.setProgress((int) progress, true);
                            } else {
                                nidPicProgressBar.setProgress((int) progress);
                            }
                        });
            }
        }

        else if (requestCode == CERTIFICATE_PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();

            if (filePath != null) {

                // Uploading the image to firebase storage
                storageReference.child("certificates_pics/" + UID + "/" + "certificate_pic.jpg")
                        .putFile(filePath)
                        .addOnSuccessListener(taskSnapshot -> {
                            // When the picked image has been uploaded successfully

                            certificatePicProgressBar.setVisibility(View.GONE);

                            storageReference.child("certificates_pics/" + UID + "/" + "certificate_pic.jpg")
                                    .getDownloadUrl()
                                    .addOnSuccessListener(uri -> generatedCertPicPath = uri.toString())
                                    .addOnFailureListener(e -> Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show());

                            // Showing the image in the restIcon view
                            try {
                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                                certificatePic.setImageBitmap(bitmap);
                                certificatePic.setVisibility(View.VISIBLE);
                            } catch (IOException e) {
                                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                                uploadNidCertificateBtn.setVisibility(View.VISIBLE);
                            }
                        })
                        .addOnFailureListener(e -> {
                            // When the uploading the picked image has failed
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                            uploadNidCertificateBtn.setVisibility(View.VISIBLE);
                        })
                        .addOnProgressListener(taskSnapshot -> {
                            // When the picked image is being uploaded

                            // Showing the progress of the upload
                            certificatePicProgressBar.setVisibility(View.VISIBLE);
                            double progress = (100 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                certificatePicProgressBar.setProgress((int) progress, true);
                            } else {
                                certificatePicProgressBar.setProgress((int) progress);
                            }
                        });
            }
        }
    }

    private void register() {

        Map<String, String> data = new HashMap<>();

        data.put("name", fullNameStr);
        data.put("icon_url", generatedIconPath);
        data.put("phone", phoneStr);
        data.put("address", addressStr);

        if (isLawyer) {
            data.put("type", "lawyer");
            data.put("specialities", specilitiesStr);
            data.put("law_firm", lawFirmStr);
            data.put("nid_number", nidStr);
            data.put("nid_pic_url", generatedNicPicPath);
            data.put("certificate_pic_url", generatedCertPicPath);
            data.put("certified", "false");

            db.getReference("Lawyers").child(UID).child("profile").setValue(data).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                    Map<String, String> hashMap = new HashMap<>();
                    hashMap.put("type", "lawyer");
                    hashMap.put("has_profile", "true");

                    db.getReference("Accounts").child(UID).setValue(hashMap);

                    userProfilePrefs.edit()
                            .putString("name", fullNameStr)
                            .putString("icon_url", generatedIconPath)
                            .putString("type", "lawyer")
                            .putString("phone", phoneStr)
                            .putString("address", addressStr)
                            .putString("law_firm", lawFirmStr)
                            .putString("specialities", specilitiesStr)
                            .putString("nid_number", nidStr)
                            .putString("nid_pic_url", generatedNicPicPath)
                            .putString("certificate_pic_url", generatedCertPicPath)
                            .apply();

                    Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            });

        } else {
            data.put("type", "citizen");

            db.getReference("Citizens").child(UID).child("profile").setValue(data).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Map<String, String> hashMap = new HashMap<>();
                    hashMap.put("type", "citizen");
                    hashMap.put("has_profile", "true");

                    db.getReference("Accounts").child(UID).setValue(hashMap);

                    userProfilePrefs.edit()
                            .putString("name", fullNameStr)
                            .putString("icon_url", generatedIconPath)
                            .putString("type", "citizen")
                            .putString("phone", phoneStr)
                            .putString("address", addressStr)
                            .apply();

                    Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }



    }

}
