package com.example.cilaw.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.cilaw.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.icon) CircleImageView icon;
    @BindView(R.id.iconProgressBar) ProgressBar iconProgressBar;
    @BindView(R.id.fullName) EditText fullName;
    @BindView(R.id.phone_view) EditText phone_view;
    @BindView(R.id.email_view) EditText email_view;
    @BindView(R.id.address_view) EditText address_view;
    @BindView(R.id.forLawyersView) LinearLayout forLawyersView;
    @BindView(R.id.specialities_view) EditText specialities_view;
    @BindView(R.id.law_firm_view) EditText law_firm_view;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        context = this;
    }
}
