package com.example.cilaw.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.cilaw.R;
import android.util.Log;
import com.opentok.*;
import com.opentok.android.Session;
import com.opentok.android.Stream;
import com.opentok.android.Publisher;
import com.opentok.android.PublisherKit;
import com.opentok.android.Subscriber;
import com.opentok.android.OpentokError;
import android.support.annotation.NonNull;
import android.Manifest;
import android.widget.FrameLayout;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class VideoCallActivity extends AppCompatActivity
        implements  Session.SessionListener, PublisherKit.PublisherListener  {

    private Context context;

    private static String API_KEY = "";
    private static String SESSION_ID = "";
    private static String TOKEN = "";
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private static final int RC_SETTINGS_SCREEN_PERM = 123;
    private static final int RC_VIDEO_APP_PERM = 124;

    private FrameLayout subscriber_container;
    private FrameLayout publisher_container;

    private Session mSession;
    private Publisher mPublisher;
    private Subscriber mSubscriber;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @AfterPermissionGranted(RC_VIDEO_APP_PERM)
    private void requestPermissions() {
        String[] perms = { Manifest.permission.INTERNET, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO };
        if (EasyPermissions.hasPermissions(context, perms)) {


            // initialize and connect to the session
            mSession = new Session.Builder(this, API_KEY, SESSION_ID).build();
            mSession.setSessionListener(this);
            mSession.connect(TOKEN);

        } else {
            EasyPermissions.requestPermissions(this, "This app needs access to your camera and mic to make video calls", RC_VIDEO_APP_PERM, perms);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_call);

        context = this;

        // initialize view objects from your layout
        subscriber_container = (FrameLayout) findViewById(R.id.subscriber_container);
        publisher_container = (FrameLayout) findViewById(R.id.publisher_container);

        SESSION_ID = "1_MX40NjM1Njc5Mn5-MTU2MTg5MTkwNzA3OX43Y1ZSa01oYkZrODB6NVQ3UnRlQ2pFZU9-fg";
        TOKEN = "T1==cGFydG5lcl9pZD00NjM1Njc5MiZzaWc9NDZmYWJlYTRiYzFlOTFhNTJmODhlZDBiMGVmYTIxN2E4ZWE4YmU2ZDpzZXNzaW9uX2lkPTFfTVg0ME5qTTFOamM1TW41LU1UVTJNVGc1TVRrd056QTNPWDQzWTFaU2EwMW9Za1pyT0RCNk5WUTNVblJsUTJwRlpVOS1mZyZjcmVhdGVfdGltZT0xNTYxODk0Mjc0Jm5vbmNlPTAuNDc1MjI1MTg2NjA0NDY0OCZyb2xlPW1vZGVyYXRvciZleHBpcmVfdGltZT0xNTY0NDg2MjY5JmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9";
        API_KEY = "46356792";

        requestPermissions();
    }

    // SessionListener methods

    @Override
    public void onConnected(Session session) {
        Log.i(LOG_TAG, "Session Connected");

        mPublisher = new Publisher.Builder(context).build();
        mPublisher.setPublisherListener(this);

        publisher_container.addView(mPublisher.getView());
        mSession.publish(mPublisher);
    }

    @Override
    public void onDisconnected(Session session) {
        Log.i(LOG_TAG, "Session Disconnected");
    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {
        Log.i(LOG_TAG, "Stream Received");

        if (mSubscriber == null) {
            mSubscriber = new Subscriber.Builder(context, stream).build();
            mSession.subscribe(mSubscriber);
            subscriber_container.addView(mSubscriber.getView());
        }
    }

    @Override
    public void onStreamDropped(Session session, Stream stream) {
        Log.i(LOG_TAG, "Stream Dropped");

        if (mSubscriber != null) {
            mSubscriber = null;
            subscriber_container.removeAllViews();
        }
    }

    @Override
    public void onError(Session session, OpentokError opentokError) {
        Log.e(LOG_TAG, "Session error: " + opentokError.getMessage());
    }

    // PublisherListener methods

    @Override
    public void onStreamCreated(PublisherKit publisherKit, Stream stream) {
        Log.i(LOG_TAG, "Publisher onStreamCreated");
    }

    @Override
    public void onStreamDestroyed(PublisherKit publisherKit, Stream stream) {
        Log.i(LOG_TAG, "Publisher onStreamDestroyed");
    }

    @Override
    public void onError(PublisherKit publisherKit, OpentokError opentokError) {
        Log.e(LOG_TAG, "Publisher error: " + opentokError.getMessage());
    }


    class GenerateToke{
        private String api_key;
        private String secret_key;
        private String session_id;
        private String token;

        public GenerateToke(String api_key, String secret_key, String session_id) {
            this.api_key = api_key;
            this.secret_key = secret_key;
            this.session_id = session_id;
        }


    }
}
