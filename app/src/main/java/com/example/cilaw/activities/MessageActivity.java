package com.example.cilaw.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cilaw.R;
import com.example.cilaw.adapters.MessagesAdapter;
import com.example.cilaw.utils.MessagesItem;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.cilaw.utils.Constants.USER_PROFILE_PREFS;

public class MessageActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.message_field) EditText message_field;
    @BindView(R.id.send_message) FloatingActionButton send_message;

    private Context context;

    private String lawyer_id = "", lawyer_name = "", lawyer_icon = "", lawyer_phone = "";
    private String citizen_id = "", citizen_name = "", citizen_icon = "", citizen_phone = "";

    private FirebaseUser user;
    private FirebaseDatabase db;
    private String UID = "";
    private String accountType;

    private SharedPreferences userProfilePrefs;

    List<MessagesItem> messagesList = new ArrayList<>();

    private String message = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = this;

        user = FirebaseAuth.getInstance().getCurrentUser();
        db = FirebaseDatabase.getInstance();
        UID = user.getUid();

        userProfilePrefs = getSharedPreferences(USER_PROFILE_PREFS, MODE_PRIVATE);
        accountType = userProfilePrefs.getString("type", "");

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));


        try {
            lawyer_id = getIntent().getStringExtra("lawyer_id");
            lawyer_name = getIntent().getStringExtra("lawyer_name");
            lawyer_icon = getIntent().getStringExtra("lawyer_icon");
            lawyer_phone = getIntent().getStringExtra("lawyer_phone");

        } catch (Exception e){

        }

        try {
            citizen_id = getIntent().getStringExtra("citizen_id");
            citizen_name = getIntent().getStringExtra("citizen_name");
            citizen_icon = getIntent().getStringExtra("citizen_icon");
            citizen_phone = getIntent().getStringExtra("citizen_phone");

        } catch (Exception ex) {}

        if (!TextUtils.isEmpty(lawyer_name)) {
            this.setTitle(lawyer_name);
        }
        if (!TextUtils.isEmpty(citizen_name)) {
            this.setTitle(citizen_name);
        }

        send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.message_menu, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        int id = item.getItemId();
//
//        if (id == R.id.action_video_call) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    private void loadMessagesCitizen() {

        try {

            db.getReference("Citizens").child(UID).child("chats").child(lawyer_id).child("messages").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    messagesList.clear();

                    for (DataSnapshot message : dataSnapshot.getChildren()) {

                        try {

                            String messageTxt = message.child("message").getValue().toString();
                            String sender = message.child("sender").getValue().toString();
                            String receiver = message.child("receiver").getValue().toString();
                            String timeStamp = message.child("timeStamp").getValue().toString();

                            messagesList.add(new MessagesItem(sender, receiver, messageTxt, timeStamp, UID));

                        } catch (Exception e) {}

                    }

                    recyclerView.setAdapter(new MessagesAdapter(context, messagesList));
                    recyclerView.scrollToPosition(recyclerView.getAdapter().getItemCount() - 1);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        } catch (Exception e) {}

    }

    private void loadMessagesLawyer() {

        try {

            db.getReference("Lawyers").child(UID).child("chats").child(citizen_id).child("messages").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    messagesList.clear();

                    for (DataSnapshot message : dataSnapshot.getChildren()) {

                        try {

                            String messageTxt = message.child("message").getValue().toString();
                            String sender = message.child("sender").getValue().toString();
                            String receiver = message.child("receiver").getValue().toString();
                            String timeStamp = message.child("timeStamp").getValue().toString();

                            messagesList.add(new MessagesItem(sender, receiver, messageTxt, timeStamp, UID));

                        } catch (Exception e) {}

                    }

                    recyclerView.setAdapter(new MessagesAdapter(context, messagesList));
                    recyclerView.scrollToPosition(recyclerView.getAdapter().getItemCount() - 1);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        } catch (Exception e) {}

    }

    @Override
    protected void onStart() {
        super.onStart();

        if (user != null) {

            db.getReference("Accounts").child(UID).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if (accountType.equalsIgnoreCase("citizen")) {
                        loadMessagesCitizen();
                    } else if (accountType.equalsIgnoreCase("lawyer")) {
                        loadMessagesLawyer();
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }
    }

    private void sendMessage() {

        message = message_field.getText().toString();

        if (!TextUtils.isEmpty(message.trim())) {

            String key;

            if (!TextUtils.isEmpty(lawyer_id)) {

                Map<String, String> lawyerObj = new HashMap<>();
                lawyerObj.put("name", lawyer_name);
                lawyerObj.put("icon_url", lawyer_icon);
                lawyerObj.put("phone", lawyer_phone);
                lawyerObj.put("last_message", message);
                lawyerObj.put("last_timeStamp", String.valueOf(System.currentTimeMillis()));

                Map<String, String> citizenObj = new HashMap<>();
                citizenObj.put("name", userProfilePrefs.getString("name", ""));
                citizenObj.put("icon_url", userProfilePrefs.getString("icon_url", ""));
                citizenObj.put("phone", userProfilePrefs.getString("phone", ""));
                citizenObj.put("last_message", message);
                citizenObj.put("last_timeStamp", String.valueOf(System.currentTimeMillis()));

                Map<String, String> messageObj = new HashMap<>();
                messageObj.put("sender", UID);
                messageObj.put("receiver", lawyer_id);
                messageObj.put("message", message);
                messageObj.put("timeStamp", String.valueOf(System.currentTimeMillis()));

                key = db.getReference("Citizens").child(UID).child("chats").child(lawyer_id).child("messages").push().getKey();

                db.getReference("Citizens").child(UID).child("chats").child(lawyer_id).child("profile_and_data").setValue(lawyerObj);
                db.getReference("Citizens").child(UID).child("chats").child(lawyer_id).child("messages").child(key).setValue(messageObj);

                db.getReference("Lawyers").child(lawyer_id).child("chats").child(UID).child("profile_and_data").setValue(citizenObj);
                db.getReference("Lawyers").child(lawyer_id).child("chats").child(UID).child("messages").child(key).setValue(messageObj);

                message_field.setText(null);

            } else if (!TextUtils.isEmpty(citizen_id)) {

                Map<String, String> lawyerObj = new HashMap<>();
                lawyerObj.put("name", userProfilePrefs.getString("name", ""));
                lawyerObj.put("icon_url", userProfilePrefs.getString("icon_url", ""));
                lawyerObj.put("phone", userProfilePrefs.getString("phone", ""));
                lawyerObj.put("last_message", message);
                lawyerObj.put("last_timeStamp", String.valueOf(System.currentTimeMillis()));

                Map<String, String> citizenObj = new HashMap<>();
                citizenObj.put("name", citizen_name);
                citizenObj.put("icon_url", citizen_icon);
                citizenObj.put("phone", citizen_phone);
                citizenObj.put("last_message", message);
                citizenObj.put("last_timeStamp", String.valueOf(System.currentTimeMillis()));

                Map<String, String> messageObj = new HashMap<>();
                messageObj.put("sender", UID);
                messageObj.put("receiver", citizen_id);
                messageObj.put("message", message);
                messageObj.put("timeStamp", String.valueOf(System.currentTimeMillis()));

                key = db.getReference("Lawyers").child(UID).child("chats").child(citizen_id).child("messages").push().getKey();

                db.getReference("Lawyers").child(UID).child("chats").child(citizen_id).child("profile_and_data").setValue(citizenObj);
                db.getReference("Lawyers").child(UID).child("chats").child(citizen_id).child("messages").child(key).setValue(messageObj);

                db.getReference("Citizens").child(citizen_id).child("chats").child(UID).child("profile_and_data").setValue(lawyerObj);
                db.getReference("Citizens").child(citizen_id).child("chats").child(UID).child("messages").child(key).setValue(messageObj);

                message_field.setText(null);

            }

        }

    }
}
