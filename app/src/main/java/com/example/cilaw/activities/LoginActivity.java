package com.example.cilaw.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.cilaw.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.cilaw.utils.Constants.AUTHENTICATION_PREFS;
import static com.example.cilaw.utils.Constants.USER_PROFILE_PREFS;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.numberView) LinearLayout numberView;
    @BindView(R.id.phoneInput) EditText phoneInput;
    @BindView(R.id.progress1) ProgressBar progress1;
    @BindView(R.id.sendCodeBtn) FloatingActionButton sendCodeBtn;

    @BindView(R.id.codeView) LinearLayout codeView;
    @BindView(R.id.sentCodeInput) EditText sentCodeInput;
    @BindView(R.id.progress2) ProgressBar progress2;
    @BindView(R.id.enterBtn) FloatingActionButton enterBtn;

    private Context context;

    private SharedPreferences userProfilePrefs, authPrefs;

    private String phoneNbrStr = "", codeStr = "";

    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private boolean mVerificationInProgress = false;

    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private FirebaseDatabase db;

    ValueEventListener valueEventListener;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        context = this;

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance();

        authPrefs = getSharedPreferences(AUTHENTICATION_PREFS, MODE_PRIVATE);
        userProfilePrefs = getSharedPreferences(USER_PROFILE_PREFS, MODE_PRIVATE);

        // Phone verification stuffs

        TextWatcher phoneNbrWatcher = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                phoneNbrStr = phoneInput.getText().toString().trim();

                if (!phoneNbrStr.startsWith("+")){
                    phoneInput.setError("Phone number must start with \"+\"");
                }
            }
        };
        phoneInput.addTextChangedListener(phoneNbrWatcher);

        sendCodeBtn.setOnClickListener(v -> {
            phoneNbrStr = phoneInput.getText().toString().trim();

            if(!phoneNbrStr.trim().isEmpty()){
                sendCodeBtn.setVisibility(View.INVISIBLE);
                progress1.setVisibility(View.VISIBLE);
                phoneInput.setEnabled(false);
                startPhoneNumberVerification(phoneNbrStr);
            }
            else{
                phoneInput.setError("Please enter phone number");
            }
        });

        enterBtn.setOnClickListener(v -> {
            codeStr = sentCodeInput.getText().toString().trim();
            if (codeStr.isEmpty()){
                sentCodeInput.setError("Field required");
            }

            else if (codeStr.length() < 6){
                sentCodeInput.setError("Invalid Code");
            }
            else{
                sentCodeInput.setEnabled(false);
                enterBtn.setVisibility(View.INVISIBLE);
                progress2.setVisibility(View.VISIBLE);
                verifyPhoneNumberWithCode(mVerificationId, codeStr);
            }
        });

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

                mVerificationInProgress = false;

                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @SuppressLint("RestrictedApi")
            @Override
            public void onVerificationFailed(FirebaseException e) {

                mVerificationInProgress = false;

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    phoneInput.setError("Invalid phone number");
                    phoneInput.setEnabled(true);
                    progress1.setVisibility(View.INVISIBLE);
                    sendCodeBtn.setVisibility(View.VISIBLE);
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...
                    Toast.makeText(context, "SMS quota exceeded", Toast.LENGTH_SHORT).show();
                } else if (e instanceof FirebaseNetworkException){
                    phoneInput.setEnabled(true);
                    progress1.setVisibility(View.INVISIBLE);
                    sendCodeBtn.setVisibility(View.VISIBLE);
                    Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
//                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = forceResendingToken;

                codeView();
            }
        };
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                120,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

//        mVerificationInProgress = true;
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @SuppressLint("RestrictedApi")
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            Log.d("LOGIN: ", "signInWithCredential:success");
                            user = task.getResult().getUser();

                            db.getReference("Accounts").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                    if (!dataSnapshot.hasChild(user.getUid())) {

                                        db.getReference("Accounts").child(user.getUid()).child("has_profile").setValue("false");

                                        Intent intent = new Intent(context, CreateProfileActivity.class);
                                        intent.putExtra("phone", phoneNbrStr);
                                        startActivity(intent);
                                        finish();

                                    } else {
                                        verifyProfile(user.getUid());
//                                        try {
//                                            boolean has_profile = (boolean) dataSnapshot.child("has_profile").getValue();
//
//                                            if (has_profile) {
//                                                verifyProfile(user.getUid());
//                                            } else {
//                                                Intent intent = new Intent(context, CreateProfileActivity.class);
//                                                intent.putExtra("phone", phoneNbrStr);
//                                                startActivity(intent);
//                                                finish();
//                                            }
//                                        } catch (Exception e) {
//                                            Log.d("Account error", e.getMessage());
//                                        }
                                    }

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                        } else if (!task.isSuccessful()){
                            // Sign in failed, display a message and update the UI
                            sentCodeInput.setEnabled(true);
                            enterBtn.setVisibility(View.VISIBLE);
                            progress2.setVisibility(View.INVISIBLE);

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                Toast.makeText(context, "Invalid Verification code", Toast.LENGTH_SHORT).show();
                            } else{
                                Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
    }

    public void codeView(){
        numberView.setVisibility(View.GONE);
        codeView.setVisibility(View.VISIBLE);
    }

    private void verifyProfile(final String UID) {

        try {

            db.getReference("Accounts").child(UID).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if (dataSnapshot.child("has_profile").getValue().toString().equalsIgnoreCase("false")) {
                        Intent intent = new Intent(context, CreateProfileActivity.class);
                        intent.putExtra("phone", phoneNbrStr);
                        startActivity(intent);
                        finish();

                    } else if (dataSnapshot.child("has_profile").getValue().toString().equalsIgnoreCase("true")) {


                        String accountType = dataSnapshot.child("type").getValue().toString();

                        authPrefs.edit()
                                .putString("UID", user.getUid())
                                .putBoolean("logged_in", true)
                                .apply();

                        if (accountType.trim().equalsIgnoreCase("citizen")) {

                            db.getReference("Citizens").child(UID).child("profile").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                    userProfilePrefs.edit()
                                            .putString("name", dataSnapshot.child("name").getValue().toString())
                                            .putString("icon_url", dataSnapshot.child("icon_url").getValue().toString())
                                            .putString("type", "citizen")
                                            .putString("phone", dataSnapshot.child("phone").getValue().toString())
                                            .putString("address", dataSnapshot.child("address").getValue().toString())
                                            .apply();

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                            Intent intent = new Intent(context, MainActivity.class);
                            startActivity(intent);
                            finish();

                        } else if (accountType.trim().equalsIgnoreCase("lawyer")) {

                            db.getReference("Lawyers").child(UID).child("profile").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                    userProfilePrefs.edit()
                                            .putString("name", dataSnapshot.child("name").getValue().toString())
                                            .putString("icon_url", dataSnapshot.child("icon_url").getValue().toString())
                                            .putString("type", "lawyer")
                                            .putString("phone", dataSnapshot.child("phone").getValue().toString())
                                            .putString("address", dataSnapshot.child("address").getValue().toString())
                                            .putString("law_firm", dataSnapshot.child("law_firm").getValue().toString())
                                            .putString("specialities", dataSnapshot.child("specialities").getValue().toString())
                                            .putString("nid_number", dataSnapshot.child("nid_number").getValue().toString())
                                            .putString("nid_pic_url", dataSnapshot.child("nid_pic_url").getValue().toString())
                                            .putString("certificate_pic_url", dataSnapshot.child("certificate_pic_url").getValue().toString())
                                            .apply();

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                            Intent intent = new Intent(context, MainActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            Toast.makeText(context, "Account unrecognisable", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(context, "Account error", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        } catch (Exception e) {
            Log.d("Account error", e.getMessage());
        }

    }
}
