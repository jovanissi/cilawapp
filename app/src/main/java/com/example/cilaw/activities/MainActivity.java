package com.example.cilaw.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.cilaw.R;
import com.example.cilaw.fragments.ChatsFragment;
import com.example.cilaw.fragments.LawyersFragment;
import com.example.cilaw.fragments.ProfileFragment;
import com.example.cilaw.utils.CallingService;
import com.example.cilaw.utils.MyService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.cilaw.utils.Constants.AUTHENTICATION_PREFS;
import static com.example.cilaw.utils.Constants.IS_ONLINE;
import static com.example.cilaw.utils.Constants.USER_PROFILE_PREFS;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.rootView) LinearLayout rootView;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tab_layout) TabLayout tab_layout;
    @BindView(R.id.viewpager) ViewPager viewPager;

    private Context context;

    private TabsPagerAdapter tabsAdapter;

    private SharedPreferences userProfilePrefs, authPrefs;

    private FirebaseDatabase db;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        context = this;

        db = FirebaseDatabase.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();

        authPrefs = getSharedPreferences(AUTHENTICATION_PREFS, MODE_PRIVATE);
        userProfilePrefs = getSharedPreferences(USER_PROFILE_PREFS, MODE_PRIVATE);

        if (user == null) {
            Intent intent = new Intent(context, LoginActivity.class);
            startActivity(intent);
            finish();

        }  else {

            String accountType = userProfilePrefs.getString("type", "");

            if (TextUtils.isEmpty(accountType)) {
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
                finish();
            }

            if (accountType.trim().equalsIgnoreCase("lawyer")) {

                String[] tabTitle = {"CHATS", "PROFILE"};
                tabsAdapter = new TabsPagerAdapter(getSupportFragmentManager(), tabTitle);

            } else {

                String[] tabTitle = {"CHATS", "PROFILE", "LAWYERS"};
                tabsAdapter = new TabsPagerAdapter(getSupportFragmentManager(), tabTitle);

            }


            viewPager.setAdapter(tabsAdapter);
            tab_layout.setupWithViewPager(viewPager);

            viewPager.setOffscreenPageLimit(tabsAdapter.getCount());

            tab_layout.setTabTextColors(ContextCompat.getColorStateList(this, R.color.tab_selector));
            tab_layout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.white));

            Intent serviceIntent = new Intent(this, MyService.class);
            startService(serviceIntent);

            Intent intent = new Intent(IS_ONLINE);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

            Intent newCallServiceIntent = new Intent(this, CallingService.class);
            startService(newCallServiceIntent);

        }

    }

    public class TabsPagerAdapter extends FragmentPagerAdapter {

        private String[] tabTitles;
        private int pageCount;

        public TabsPagerAdapter(FragmentManager fm, String[] tabTitles) {
            super(fm);

            this.tabTitles = tabTitles;
            this.pageCount = tabTitles.length;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new ChatsFragment();
                case 1:
                    return new ProfileFragment();
                case 2:
                    return new LawyersFragment();
            }
            return null;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        @Override
        public int getCount() {
            return this.pageCount;
        }
    }
}
