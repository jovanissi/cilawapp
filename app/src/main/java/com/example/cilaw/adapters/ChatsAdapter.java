package com.example.cilaw.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cilaw.R;
import com.example.cilaw.activities.MessageActivity;
import com.example.cilaw.utils.ChatsItem;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;
import static com.example.cilaw.utils.Constants.USER_PROFILE_PREFS;

public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.ViewHolder> {

    private Context context;
    private List<ChatsItem> chatsItemsList;

    private SharedPreferences userProfilePrefs;
    private String type;

    public ChatsAdapter(Context context, List<ChatsItem> chatsItemsList) {
        this.context = context;
        this.chatsItemsList = chatsItemsList;
    }

    @NonNull
    @Override
    public ChatsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(context).inflate(R.layout.chats_item, viewGroup, false);

        userProfilePrefs = context.getSharedPreferences(USER_PROFILE_PREFS, MODE_PRIVATE);
        type = userProfilePrefs.getString("type", "");

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatsAdapter.ViewHolder viewHolder, int i) {

        final ChatsItem chatsItem = chatsItemsList.get(i);

        viewHolder.receiverName.setText(chatsItem.getName());
        viewHolder.lastMessage.setText(chatsItem.getLastMessage().trim());

        Date date = new Date(Long.parseLong(chatsItem.getLastMessageTime()));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm  dd MMM, yyyy");

        viewHolder.lastMessageTime.setText(simpleDateFormat.format(date));

        try {
            Picasso.with(context).load(chatsItem.getIconUrl()).placeholder(R.drawable.pp).into(viewHolder.icon);

        } catch (Exception e) {}

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, MessageActivity.class);

                if (type.equalsIgnoreCase("citizen")) {
                    intent.putExtra("lawyer_id", chatsItem.getId());
                    intent.putExtra("lawyer_name", chatsItem.getName());
                    intent.putExtra("lawyer_icon", chatsItem.getIconUrl());
                    intent.putExtra("lawyer_phone", chatsItem.getPhone());
                    context.startActivity(intent);

                } else if (type.equalsIgnoreCase("lawyer")) {
                    intent.putExtra("citizen_id", chatsItem.getId());
                    intent.putExtra("citizen_name", chatsItem.getName());
                    intent.putExtra("citizen_icon", chatsItem.getIconUrl());
                    intent.putExtra("citizen_phone", chatsItem.getPhone());
                    context.startActivity(intent);

//                    Toast.makeText(context, chatsItem.getId(), Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return chatsItemsList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.icon) CircleImageView icon;
        @BindView(R.id.receiverName) TextView receiverName;
        @BindView(R.id.lastMessageTime) TextView lastMessageTime;
        @BindView(R.id.lastMessage) TextView lastMessage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
