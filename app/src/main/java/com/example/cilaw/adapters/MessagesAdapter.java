package com.example.cilaw.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.cilaw.R;
import com.example.cilaw.utils.MessagesItem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {

    private Context context;
    private List<MessagesItem> messagesItemList;

    public MessagesAdapter(Context context, List<MessagesItem> messagesItemList) {
        this.context = context;
        this.messagesItemList = messagesItemList;
    }

    @NonNull
    @Override
    public MessagesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.messages_item, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MessagesAdapter.ViewHolder viewHolder, int i) {

        MessagesItem message = messagesItemList.get(i);

        String sender = message.getSendId();
        String receiver = message.getReceiverId();
        String messageTxt = message.getMessageTxt();
        String timeStamp = message.getTimeStamp();
        String myUid = message.getMyUid();

        Date date = new Date(Long.parseLong(timeStamp));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm  dd MMM, yyyy");

        if (sender.equals(myUid)) {
            viewHolder.recBubble.setVisibility(View.GONE);
            viewHolder.sentBubble.setVisibility(View.VISIBLE);
            viewHolder.sentMessage.setText(messageTxt);
            viewHolder.sentTime.setText(simpleDateFormat.format(date));

        } else if (receiver.equals(myUid)) {
            viewHolder.sentBubble.setVisibility(View.GONE);
            viewHolder.recBubble.setVisibility(View.VISIBLE);
            viewHolder.recMessage.setText(messageTxt);
            viewHolder.recTime.setText(simpleDateFormat.format(date));
        }

    }

    @Override
    public int getItemCount() {
        return messagesItemList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.sentBubble) LinearLayout sentBubble;
        @BindView(R.id.sentMessage) TextView sentMessage;
        @BindView(R.id.sentTime) TextView sentTime;
        @BindView(R.id.progress) ProgressBar progress;

        @BindView(R.id.recBubble) LinearLayout recBubble;
        @BindView(R.id.recMessage) TextView recMessage;
        @BindView(R.id.recTime) TextView recTime;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
