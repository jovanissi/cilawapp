package com.example.cilaw.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.cilaw.R;
import com.example.cilaw.activities.MessageActivity;
import com.example.cilaw.activities.VideoChatAgoraActivity;
import com.example.cilaw.utils.LawyersItem;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class LawyersAdapter extends RecyclerView.Adapter<LawyersAdapter.ViewHolder> {

    private Context context;
    private List<LawyersItem> lawyersItemList;

    public LawyersAdapter(Context context, List<LawyersItem> lawyersItemList) {
        this.context = context;
        this.lawyersItemList = lawyersItemList;
    }

    @NonNull
    @Override
    public LawyersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v= LayoutInflater.from(context).inflate(R.layout.lawyers_item, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull LawyersAdapter.ViewHolder viewHolder, int i) {

        final LawyersItem lawyer = lawyersItemList.get(i);

        viewHolder.lawyerName.setText(lawyer.getName());
        viewHolder.lawyerPhone.setText(lawyer.getPhone());
        viewHolder.address.setText(lawyer.getAddress());
        viewHolder.specialities.setText(lawyer.getSpecialities());
        viewHolder.lawFirm.setText(lawyer.getLawFirm());

        try {
            Picasso.with(context).load(lawyer.getIconUrl()).placeholder(R.drawable.pp).into(viewHolder.icon);
        } catch (Exception e) {}

        viewHolder.callBtn.setOnClickListener(v -> {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + lawyer.getPhone()));
            context.startActivity(callIntent);
        });

        viewHolder.messageBtn.setOnClickListener(v -> {
            Intent intent = new Intent(context, MessageActivity.class);
            intent.putExtra("lawyer_id", lawyer.getUid());
            intent.putExtra("lawyer_name", lawyer.getName());
            intent.putExtra("lawyer_icon", lawyer.getIconUrl());
            intent.putExtra("lawyer_phone", lawyer.getPhone());
            context.startActivity(intent);
        });

        viewHolder.videoCallBtn.setOnClickListener(v -> {
            Intent intent = new Intent(context, VideoChatAgoraActivity.class);
            intent.putExtra("call_to_id", lawyer.getUid());
            intent.putExtra("calling", true);
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return lawyersItemList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.icon) CircleImageView icon;
        @BindView(R.id.lawyerName) TextView lawyerName;
        @BindView(R.id.lawyerPhone) TextView lawyerPhone;
        @BindView(R.id.specialities) TextView specialities;
        @BindView(R.id.lawFirm) TextView lawFirm;
        @BindView(R.id.address) TextView address;
        @BindView(R.id.messageBtn) ImageButton messageBtn;
        @BindView(R.id.callBtn) ImageButton callBtn;
        @BindView(R.id.videoCallBtn) ImageButton videoCallBtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
